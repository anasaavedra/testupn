﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TC3
{
    class Nodo
    {
        public string id;
        public string nombre;
        public int puntos;
        public Nodo siguiente;        
    }
}
